<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRequest;
use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index()
    {
        return Car::all();
    }

    /**
     * @param CarRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        $car = Car::create($request->validated());

        if ($car) {
            return response(['status' => 'ok']);
        }

        return response(['status' => 'failed']);
    }

    /**
     * @param Car $car
     * @return mixed
     */
    public function show(Car $car)
    {
        return Car::findOrFail($car);
    }

    /**
     * @param CarRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CarRequest $request, $id)
    {
        $car = Car::findOrFail($id);

        $car->fill($request->all());
        $car->save();

        return response()->json($car);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::findOrFail($id);

        if ($car->delete()) {
            return response(null, 204);
        }

        return response(['status' => 'failed']);
    }
}
