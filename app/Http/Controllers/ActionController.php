<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActionRequest;
use App\Models\Action;

class ActionController extends Controller
{

    public function index()
    {
        return Action::all();
    }

    /**
     * @param ActionRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(ActionRequest $request)
    {
        try {
            $action = Action::create($request->validated());
        } catch (\Exception $e) {
            if ($e->getCode() == 23000) {
                return response(['status' => 'failed', 'error' => 'car or user is already busy']);
            }
        }

        if ($action) {
            return response(['status' => 'ok']);
        }

        return response(['status' => 'failed']);
    }

    /**
     * @param Action $action
     * @return mixed
     */
    public function show(Action $action)
    {
        return Action::findOrFail($action);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Action::findOrFail($id);

        if ($user->delete()) {
            return response(null, 204);
        }

        return response(['status' => 'failed']);
    }
}
