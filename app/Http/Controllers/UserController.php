<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    /**
     * @param UserRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create($request->validated());

        if ($user) {
            return response(['status' => 'ok']);
        }

        return response(['status' => 'failed']);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function show(User $user)
    {
        return User::findOrFail($user);
    }

    /**
     * @param UserRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $user->fill($request->all());
        $user->save();

        return response()->json($user);
    }

    /**
     * @param $id
     * @return Application|ResponseFactory|Response|string
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->delete()) {
            return response(null, 204);
        }

        return response(['status' => 'failed']);
    }
}

